# Start with a base image containing Java runtime and Maven
FROM maven:3.9.6-amazoncorretto-21-al2023 as build
# Copy the project files to the container
COPY src /home/app/src
COPY pom.xml /home/app

# Build the application
RUN mvn -f /home/app/pom.xml clean package

# Use OpenJDK for running the application
FROM openjdk:21
COPY --from=build /home/app/target/*.jar app.jar

# Expose the port the app runs on
EXPOSE 8085

# Run the jar file
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
