# java-spring-api

 a simple project with java spring for search album and book from google api and itunes api

## Getting started


1 . 
after clone this project you must config **application.yml**  file
- limit for request limit to show
- key for google api key 
- port for redis and spring boot (default is 8085 for spring and 6379 for redis)

2 . build docker 


```http
    docker-compose build
```
3 . run docker

```http
    docker-compose up
```

4 . after run successfuly you can use Api

| Parameter | Type     | Description                       |
| :-------- | :------- |:----------------------------------|
| `query` | `string` | hot key for search album and book |

#### do search with api

```http
  GET /api/search?hotkey={query}
```
for example :
```http
http://localhost:8085/api/search?hotkey=Harry Potter
```


see swagger api doc (UI) :
```http
/swagger-ui/index.html
```

or api :
```http
GET /v3/api-docs
```
