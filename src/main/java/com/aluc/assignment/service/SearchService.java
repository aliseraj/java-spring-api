package com.aluc.assignment.service;

import com.aluc.assignment.util.Album;
import com.aluc.assignment.util.AlbumSearchResult;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.netty.handler.logging.LogLevel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.ClientCodecConfigurer;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.netty.transport.logging.AdvancedByteBufFormat;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@Slf4j
@Service
public class SearchService {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private WebClient.Builder webClientBuilder;

    public SearchService() {
        this.restTemplate = new RestTemplate();
    }
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Value("${api.limit}")
    private int apiLimit;
    @Value("${google.key}")
    private String googleKey;

    public JsonNode searchBooks(String query) {
        try {


        WebClient webClient = WebClient.builder()
                .baseUrl("https://www.googleapis.com/")
                .defaultHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/117.0")
                .build();

        JsonNode result = webClient.get()
                .uri(uriBuilder -> uriBuilder.path("/books/v1/volumes")
                        .queryParam("q",query+":keys")
                        .queryParam("key",googleKey)
                        .queryParam("maxResult",apiLimit)
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(JsonNode.class)
                .map(s -> {
                    ArrayNode booksArray = (ArrayNode) s.path("items");
                    ArrayNode formattedBooksArray = JsonNodeFactory.instance.arrayNode();
                    for (JsonNode book : booksArray) {
                        ObjectNode formattedBook = JsonNodeFactory.instance.objectNode();
                        JsonNode volumeInfo = book.path("volumeInfo");
                        formattedBook.put("title", volumeInfo.path("title").asText());
                        formattedBook.put("subtitle", volumeInfo.path("subtitle").asText());
                        formattedBook.set("authors", volumeInfo.path("authors"));
                        formattedBook.put("publishedDate", volumeInfo.path("publishedDate").asText());
                        formattedBook.put("publisher", volumeInfo.path("publisher").asText());
                        formattedBook.put("description", volumeInfo.path("description").asText());
                        formattedBook.put("pageCount", volumeInfo.path("pageCount").asInt());
                        formattedBook.set("categories", volumeInfo.path("categories"));
                        formattedBooksArray.add(formattedBook);
                    }
                    ObjectNode resultObject = JsonNodeFactory.instance.objectNode();
                    resultObject.set("books", formattedBooksArray);
                    return resultObject;
                })
                .block();

        List<JsonNode> dataNodes = result.findParents("title");
        //sort it
        List<JsonNode> sortedDataNodes = dataNodes
                .stream()
                .sorted(Comparator.comparing(o -> o.get("title").asText()))
                .collect(Collectors.toList());
        ArrayNode arrayNode = objectMapper.createObjectNode().arrayNode().addAll(sortedDataNodes);
        return objectMapper.createObjectNode().set("empData", arrayNode);}
        catch (Exception e) {
            // Return a custom error message as google api

            ObjectNode errorObject = JsonNodeFactory.instance.objectNode();

            e.printStackTrace(); //print error in command
            errorObject.put("error", "An error occurred . Please try again later. or check the log -_- ");
            return errorObject;
        }

    }


    private void acceptedCodecs(ClientCodecConfigurer clientCodecConfigurer) {
        ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        clientCodecConfigurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024);
        clientCodecConfigurer.customCodecs().registerWithDefaultConfig(new Jackson2JsonDecoder(objectMapper, MimeType.valueOf("text/javascript; charset=utf-8")));
        clientCodecConfigurer.customCodecs().registerWithDefaultConfig(new Jackson2JsonEncoder(objectMapper, MimeType.valueOf("text/javascript; charset=utf-8")));
    }

    public AlbumSearchResult searchAlbums(String query) {
        try {
            HttpClient httpClient = HttpClient.create().wiretap(this.getClass().getCanonicalName(), LogLevel.TRACE, AdvancedByteBufFormat.TEXTUAL);
            WebClient webClient = WebClient.builder()
                    .clientConnector(new ReactorClientHttpConnector(httpClient))
                    .baseUrl("https://itunes.apple.com")
                    .defaultHeader(HttpHeaders.USER_AGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/117.0")
                    .exchangeStrategies(ExchangeStrategies.builder().codecs(this::acceptedCodecs).build())
                    .build();

        AlbumSearchResult result = webClient.get()
                .uri(uriBuilder -> uriBuilder.path("/search")
                        .queryParam("term", query)
                        .queryParam("limit", apiLimit)
                        .queryParam("entity", "album")
                        .build())
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(AlbumSearchResult.class)
                .block();

        List<Album> collect = result.getResults()
                .stream()
                .sorted(Comparator.comparing(Album::getCollectionName))
                .collect(Collectors.toList());

        result.setResults(collect);

        return result;
        }
        catch (Exception e) {
            e.printStackTrace();

            // Return a custom error message as apple api
            AlbumSearchResult errorResult = new AlbumSearchResult();
            List<String> errors = new ArrayList<>();

            errors.add("An error occurred . Please try again later. or check the log -_-");
            errorResult.setErrors(errors);
            return errorResult;
        }
    }
}

