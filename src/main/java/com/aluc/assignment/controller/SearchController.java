package com.aluc.assignment.controller;


import com.aluc.assignment.service.SearchService;
import com.aluc.assignment.util.AlbumSearchResult;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class SearchController {

    private final SearchService searchService;

    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }
    @GetMapping("/api/test")
    public ResponseEntity<?> get() {
        return ResponseEntity.ok("ok");
    }


    @GetMapping("/api/search")
    public ResponseEntity<Object> search(@RequestParam(value = "hotkey", defaultValue = "book") String hotkey) throws IOException {
        String query = hotkey;
        AlbumSearchResult albumSearchResult = searchService.searchAlbums(query);
        JsonNode bookSearchResult = searchService.searchBooks(query);


        SearchResponse searchResponse = new SearchResponse( albumSearchResult,bookSearchResult);
        return ResponseEntity.ok(searchResponse);
    }


    public class SearchResponse {

        private Object albums;
        private Object books;

        public SearchResponse(Object albums, Object books) {
            this.albums = albums;
            this.books = books;
        }

        public Object getAlbums() {
            return albums;
        }

        public void setAlbums(Object albums) {
            this.albums = albums;
        }

        public Object getBooks() {
            return books;
        }

        public void setBooks(Object books) {
            this.books = books;
        }
    }
}
