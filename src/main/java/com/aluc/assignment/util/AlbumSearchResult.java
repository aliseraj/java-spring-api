package com.aluc.assignment.util;

import com.aluc.assignment.util.Album;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AlbumSearchResult {
//    private int resultCount;
    private List<Album> results = new ArrayList<>();
    private List<String> errors;

}